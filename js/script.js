/* Author:
	Catalin Saveanu
*/
$(document).ready(function() {
	var indx=0;
	var texts=[];


	function switchText(){
		indx++;
		if (indx>texts.length-1) indx=0;
		$('h1').fadeTo(400, 0.01, function(){
			$(this).html(texts[indx]).fadeTo(400, 1);
		});
	};

	texts.push($('h1').html());
	$('#h1-txts p').each(function(indx){
		texts.push($(this).html())
	});

	if (screen.width>480){
		$('ul li').each(function(indx){
			var text=$(this).find('a').html();
			if (text=="Email us at hello@wearex3.com") text="Email us at <strong>hello@wearex3.com</strong>";
			var $tooltip=$("<div class='tooltip'><em>"+text+"</em></div>").appendTo($(this));
		});
		$('ul a').hover(
			function(e) {
				e.preventDefault();
				$(this).stop().animate({'opacity':'1'});
				$span=$(this).parent().find('.tooltip');
				$span.stop().show().css({'top': '-62px','opacity':'0'}).animate({'top':'-47px', 'opacity':'1'});
			},
			function(e) {
				e.preventDefault();
				$(this).stop().animate({'opacity':'0'});
				$span=$(this).parent().find('.tooltip');
				$span.stop().fadeOut();
			}
		);
	}




	setInterval(function(){switchText()},5000);
});






